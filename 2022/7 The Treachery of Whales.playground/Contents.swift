import UIKit

let crabPositions = [16,1,2,0,4,2,7,1,2,14]
var minTotalFuelCost = Int.max
var bestAlignment = 0


for i in 0..<crabPositions.count {
    var totalFuelCostToMoveAllCrabsToPositionI = 0
    var stepCost = 0


    for j in 0..<crabPositions.count {
        totalFuelCostToMoveAllCrabsToPositionI += abs(crabPositions[i] - crabPositions[j])
    }

    if totalFuelCostToMoveAllCrabsToPositionI < minTotalFuelCost {
        minTotalFuelCost = totalFuelCostToMoveAllCrabsToPositionI
        bestAlignment = i
    }

}
print("minTotalFuelCost \(minTotalFuelCost)")
print("bestAlignment \(bestAlignment)")
