import Foundation

protocol Solvable {
    func parse(input: String)
    func solve(input: String) -> String
}
