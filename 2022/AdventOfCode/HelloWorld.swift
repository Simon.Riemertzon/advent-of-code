//
//  File.swift
//  AdventOfCode
//
//  Created by Simon Riemertzon on 12/01/2024.
//

import Foundation

class HelloWorld {

    var input = 0

    public init(number: Int) {
        input = number
    }

}
