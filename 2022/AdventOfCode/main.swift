//
//  main.swift
//  AdventOfCode
//
//  Created by Simon Riemertzon on 12/01/2024.
//

import Foundation

let day13Description = "--- Day 13: Transparent Origami --- You reach another volcanically active part of the cave. It would be nice if you could do some kind of thermal imaging so you could tell ahead of time which caves are too hot to safely enter. Fortunately, the submarine seems to be equipped with a thermal camera! When you activate it, you are greeted with: Congratulations on your purchase! To activate this infrared thermal imaging camera system, please enter the code found on page 1 of the manual. Apparently, the Elves have never used this feature. To your surprise, you manage to find the manual; as you go to open it, page 1 falls out. It's a large sheet of transparent paper! The transparent paper is marked with random dots and includes instructions on how to fold it up (your puzzle input). For example:"

let day13Example = "6,10 0,14 9,10 0,3 10,4 4,11 6,0 6,12 4,1 0,13 10,12 3,43,08,41,102,148,109,0 fold along y=7 fold along x=5"
let puzzleInput = "URL"

struct Day13 {
    func parseCoordinates(_ input: String) -> [(Int, Int)] {
        let lines = input.split(separator: "\n")
        let coordinates = lines.map { line -> (Int, Int) in
            let parts = line.split(separator: ",").map { Int($0)! }
            return (parts[0], parts[1])
        }
        return coordinates
    }

    //    func solvePart1(input: String) -> [(Int, Int)] {
    //        return
    //    }
    //
    //    func solvePart2(input: String) -> String {
    //        return ""
    //    }
}
import Foundation

struct Day14 {
    func parse(input: String) -> String {

        var points: [(Int, Int)] = []
        let lines = input.split(separator: "\n")
        for line in lines {
            let positions = line.split(separator: " ").map { String($0) }.filter { !$0.contains("->") }

            let coordinates = positions.map { line -> (Int, Int) in
                let parts = line.split(separator: ",").map { Int($0)! }
                return (parts[0], parts[1])
            }
            print(coordinates)
            points.append(contentsOf: coordinates)
        }
        return String("\(points.count)")
    }

    //
    //    func chatGPT(input: String) -> Int {
    //        let input = """
    //        498,4 -> 498,6 -> 496,6
    //        503,4 -> 502,4 -> 502,9 -> 494,9
    //        """
    //
    //        // Split the input into lines
    //        let lines = input.split(separator: "\n")
    //
    //        // Initialize an empty array to hold all points
    //        var points: [(Int, Int)] = []
    //
    //        // Iterate over each line
    //        for line in lines {
    //            // Split the line into point strings
    //            let pointStrings = line.split(separator: " ").map { String($0) }.filter { !$0.contains("->") }
    //
    //            // Map each point string to a tuple and append to the points array
    //            let linePoints = pointStrings.map { pointString -> (Int, Int) in
    //                let components = pointString.split(separator: ",").map { Int($0)! }
    //                return (components[0], components[1])
    //            }
    //
    //            points.append(contentsOf: linePoints)
    //        }
    //
    //        // Now `points` contains all the coordinates as tuples
    //        print(points)
    //        return points[0]
    //
    //
    //}

}


struct Day15 {
    // 1. Parse the input into a array.
    // 2. Start looping through the array
    // 3. Save the lowest path so far
    // 4. Iterate through each
    // 5.

    func parse(input: String) -> String{
        // Split the string into an array of rows
        let rows = input.split(separator: "\n")

        // Map each row to an array of integers
        let grid: [[Int]] = rows.map { row in
            row.compactMap { char in
                Int(String(char)) // Safely convert each character to an Int
            }
        }

        // Loop through each row
        for (y, row) in grid.enumerated() {

            // Loop through each column in the current row
            for (x, value) in row.enumerated() {
                // Print the value with a space, but do not add a newline at the end
                print(value, terminator: " ")

                // When we reach the end of a row, print a newline character
                if x == row.count - 1 {
                    print("") // This will move to the next line
                }
            }
        }
     return "40"
    }

}
