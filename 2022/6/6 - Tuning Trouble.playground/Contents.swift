import UIKit

findMarker("bvwbjplbgvbhsrlpgdmjqwftvncz", 14)
func findMarker(_ input: String, _ distinctCharacters: Int) -> Int {

    let inputChars = input.map { $0 }
    var array = [Character]()

    for (index, char) in inputChars.enumerated() {
        array.append(char)
        if array.count >= distinctCharacters {
            let set = Set(array)
            set
            if set.count < distinctCharacters {
                array.removeFirst()
                set
                continue
            } else {
                return index + 1
            }
        }
    }
    return -1
}


