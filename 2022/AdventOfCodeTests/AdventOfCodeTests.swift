//
//  AdventOfCodeTests.swift
//  AdventOfCodeTests
//
//  Created by Simon Riemertzon on 12/01/2024.
//
import XCTest

final class AdventOfCodeTests: XCTestCase {
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInputEquals() throws {
        XCTAssertEqual(2 + 2, 4, "Math still works!")
        XCTAssertEqual(10, HelloWorld(number: 10).input)
    }

    func testInputNotEquals() throws {
        XCTAssertNotEqual(10, HelloWorld(number: 1).input)
    }






















































    func testDay14() {
        let day = Day14()
        print(day.parse(
            input:"""
            498,4 -> 498,6 -> 496,6
            503,4 -> 502,4 -> 502,9 -> 494,9
            """
        ))
    }

    func testDayChallenges() {
        let testCases = [
            ("Day15", input: """
                1163751742
                1381373672
                2136511328
                3694931569
                7463417111
                1319128137
                1359912421
                3125421639
                1293138521
                2311944581
            """, expected: "40"),
            ("Day14", input: """
             498,4 -> 498,6 -> 496,6
             503,4 -> 502,4 -> 502,9 -> 494,9
             """, expected: "24")
        ]

        for (day, input, expected) in testCases {
            let result = performChallenge(day: day, input: input)
            XCTAssertEqual(result, expected, "Expected \(expected) for \(day), but got \(result)")
        }
    }

    func performChallenge(day: String, input: String) -> String {
        switch day {
        case "Day15": return Day15().parse(input: input)
        case "Day14": return Day14().parse(input: input)
        default:
            print("No Day Associated with \(day)")
        }

        // Here, call the function related to the day's challenge and return its result
        // This is a placeholder. You'll implement the actual logic in your Challenges group.
        return ""
    }
}
