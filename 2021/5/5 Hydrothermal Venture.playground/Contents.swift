import UIKit
import Foundation

let file = "Day5"

var result = ""
var input = [""]

if let inputURL = Bundle.main.url(forResource: file, withExtension: ".input") {
    if let startInput = try? String(contentsOf: inputURL) {
        input = startInput.components(separatedBy: "\n")
        let removeCharacters: Set<Character> = ["\"", "-", ">", " "]
        var coordinate : [Int]
        for (index, element) in input.enumerated() {
            var filtered = element
            filtered.removeAll(where: removeCharacters.contains)
            coordinate.append(Int(filtered))
        }


    }
    print()
}

