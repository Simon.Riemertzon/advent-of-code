import UIKit

let input = """
[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
"""
//part1(input: input)
part2(input: input)

func part2(input: String) {
    let pairs: [Character: Character] = [
        "(": ")",
        "[": "]",
        "{": "}",
        "<": ">"
    ]

    var pointSum: Int = 0
    var open : [Character] = []
    var closing : [Character] = []

    let lines = input.split(separator: "\n")

    for (_, line) in lines.enumerated() {

        for (charIndex, char) in line.enumerated() {
            print(charIndex)
            print(char)

            let last = open.last // Can be nil

            if let last {
                let closing = pairs[last]
                if char == closing {
                    open.removeLast()
                } else { // Corrupt line, discard everything.
                    open.removeAll()
                    break
                }
            }
            if ["(", "[", "{", "<"].contains(char) {
                open.append(char)
            }

            if charIndex == line.count - 1{
                let hello = "Hello"
                pointSum += 1

            }
        }
    }

    print(pointSum)
}








func part1(input: String ) {


    let pairs: [Character: Character] = [
        "(": ")",
        "[": "]",
        "{": "}",
        "<": ">"
    ]

    let illegalCharPoints: [Character: Int] = [
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137
    ]

    var pointSum: Int = 0
    var open : [Character] = []

    input.split(separator: "\n").forEach { line in

        for char in line {
            let last = open.last // Can be nil

            if let last {
                let closing = pairs[last]
                if char == closing {
                    open.removeLast()
                } else {
                    if let points = illegalCharPoints[char] {
                        pointSum += points
                        break
                    }
                }
            }
            if ["(", "[", "{", "<"].contains(char) {
                open.append(char)
            }
        }
    }
    print(pointSum)
}
